==External patches==
Magic fix (by Canofworms)
Music Player names fix (by Mateo)

==Knight==
Bladeblitz replaces Equip Armor.
Equip Shields is now the Level 5 ability.

==Thief==
Long Reach replaces Find Passages. Find Passages is still innate.
Light Step replaces Sprint. Sprint is still innate.
HP+10% replaces Vigilance. Vigilance is still innate.

==Monk==
!Spellblade3 replaces Counter. Counter is still innate.
!Drink replaces HP + 10%
EXP Up replaces HP + 20%

==White Mage==
Can now equip Hammers
Gains !Revive instead of MP +10% as its capstone

==Blue Mage==
Scan is now a Level 1 ability
MP +30% is the new capstone

==Red Mage==
Strength upped to 37 (was 32)
!Red3 is the Level 1 ability
!Focus is the Level 2 ability
!Time4 is the Level 3 ability
Reduced the ABP needed to master X-Magic to 400

==Berserker==
Can now have 3 innate abilities equipped.

==Summoner==
!Call is now the Level 1 ability. The 5 levels of Summon are moved up 1 level.

==Ninja==
!Black3 replaces First Strike. First Strike is still innate.

==Geomancer==
Strength raised to 32 (was 27)
Can now equip Axes
Equip Axes replaces Light Step. Light Step is still innate.

==Samurai==
Translated ability names, which were simply romanized in Vanilla Advance.

==Dancer==
Vitality raised to 20 (was 14)

==Mime==
Reduced the ABP needed to master Mimic to 255.

==Advance jobs==
The 3 GBA jobs now have innate Sprint.
Oracle can now equip Hammers.
Gladiator can no longer equip Knight Swords.
Gladiator's main ability is now Bladeblitz.

==Equipment names==
Main Gauche -> Guardian
Ultima Weapon -> Atma Weapon
Red Slippers -> Red Shoes

==Equipment==
Flametounge, Icebrand and Brave Blade are now a normal Sword rather than a Knight Sword.
Starting in 1.27, Oricalchum Dirk is replaced with Thunder Blade, a Thunder-elemental Knife.
Gold Hairpin, Knife and Dagger are now usable by all classes.
Crystal Shield now has Coral Ring elemental properties and immunity to Toad and Paralyze.

==Ability names==
Zeninage -> Gil Toss
Mineuchi -> Blade Bash
Iainuki -> Slash
Shirahadori -> Evade
Rapid Fire -> X-Fight
Dualcast-> X-Magic

==Enemy names==
Cait Sith (Metamorph) -> Gaelicat
Wendigo -> Stalker
Archeodemon -> Atma Weapon
Guardian -> Vegnagun

==Enemy steals==
Gogo's Leather Armor is replaced with a second Mirage Vest.
Starting in 1.27, White Serpent has Daggers in both Common and Rare steal slots.
Starting in 1.27, Bandersnatch has a Rare Steal of a Hi-Potion.
Starting in 1.27, Iron Claw has a Dark Matter as a Common Steal and a Fuma Shuriken as a Rare Steal
Starting in 1.27, Ricard Mages have Frost Rod a rare steal

==Enemy drops==
The human form of Liquid Flame now drops a Flame Rod.
The hand form of Liquid Flame now drops a Flame Shield.

==Enemy data==
Reduced Omega's evade from 95% to 65%
Removed Exdeath's unused Meteo AI phase from his Meatfort battle
Replaced Atma Weapon's Death counter with a 33% each chance of Firaga, Haste or Blink.

==Chests==
The Karnak 500 chest that once contained a Thunder Scroll now contains a Thunder Rod.
The Thief's Glove is moved to the Chest in Ronka that contained a second Main Gauche. 
The chest the Thief's Gloves once occupied now holds Blizzara.
The chest in Bal that contained a Hero Cocktail now has a Gaia Gear.
A Phoenix Down chest in Drakenvale now holds Romeo's Ballad.
A Reflect Ring replaces the Blood Sword as a Monster-in-a-Box in Barrier Tower.
A Gold Hairpin replaces a Red Shoes pair guarded by 3 old men.

==Shops==
The World 2 magic shops now have some World 1 spells so as to not screw over #Fifthjob users.
Mirage Village shop now sells Buckshot and Blastshot.

==Special Thanks==
Square Enix for making this awesome game.
Nintendo for their localization of FFV.
Can of Worms for helping with hex editing.
The Great Waddler for his balance patch, which provided inspiration.
The FFVGoons Discord for their friendliness.

==Version History==
1.0 - initial release
1.1 - Added 3 Universal Weapons, fixed Unarmed Spellblade and raised Dancer Vitality.
1.11 - Moved Drink on Monk to 100 ABP and Spellblade3 on Monk to 60 ABP, updated Mirage shops.
1.21 - Updated chests
1.27 - Updated steal list, fixed bug that prevented Red Mage from getting Focus, added Thunder Blade